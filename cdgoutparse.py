#! /usr/bin/env python
# -*- coding:utf-8 -*-
# cdgoutparse - a parser for .cda files used by natural speech parsers
# Copyright (C) 2010  Arne Köhn <arne@arne-koehn.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from ply import yacc

class CdaSpec(object):
    def __init__(self,type,key,value,pointer):
        self.type = type
        self.key = key
        self.value = value
        self.pointer = pointer
    def __cmp__(self, other):
        return cmp(self.__dict__, other.__dict__)
    def __str__(self, sentence = None):
        if self.type == "tag":
            return "'%s' / '%s'"%(self.key, self.value.replace("\\", "\\\\").replace("'", r"\'"))
        else:
            result = "'%s' -> '%s' -> %i"%(self.key, self.value.replace("\\", "\\\\").replace("'", r"\'"), self.pointer)
            if (not sentence is None) and (self.pointer > 0):
                result += " // ( "+sentence[self.pointer - 1].word+" )"
            return result

    
class CdaWord(object):
    def __init__(self, word, start, end, specs):
        self.specs = specs
        self.word = word
        self.start = start
        self.end = end
    def __cmp__(self, other):
        res = cmp(self.word, other.word)
        if res:
            return res
        return cmp(self.specs, other.specs)
    def __str__(self):
        res =  "%i %i %s\n"%(self.start, self.end, self.word)
        for spec in self.specs.items():
            res += (str(spec[1])+"\n")
        return res
    def str_with_comment(self, sentence):
        ''' same as __str__, but adds the word pointed to in specs as a comment.
        e.g.: SYN -> NN -> 4 // frobnicate
        '''
        res =  "%i %i '%s'\n"%(self.start, self.end, self.word.replace("\\", "\\\\").replace("'", r"\'"))
        for spec in self.specs.items():
            res += (spec[1].__str__(sentence)+"\n")
        return res

    
class ParseException(Exception):
    def __init__(self,p):
        self.line = p.lineno
        self.token = p.value

# Get the token map from the lexer.  This is required.
from .cdgoutlex import tokens, lexer

def p_AnnoEntry(p):
    'AnnoEntry : STR COLON STR BIIMPL AnnoList SEMICOLON'
    # Dirty hack to change the line numbers for better debugging.
    lexer.lineno = 0
    p[0] = p[5]

def p_AnnoList(p):
    'AnnoList : AnnoList COMMA Annotation'
    p[1].append(p[3])
    p[0] = p[1]

def p_AnnoList_single(p):
    'AnnoList : Annotation'
    p[0] = [p[1]]

def p_Annotation(p):
    'Annotation : NUMBER NUMBER STR SpecSeq'
    p[0] = CdaWord(p[3], p[1], p[2], p[4])

def p_SpecSeq(p):
    'SpecSeq : SpecSeq Specification'
    p[1][p[2].key] =p[2]
    p[0] = p[1]

def p_SpecSeq_empty(p):
    'SpecSeq :'
    p[0] = {}

def p_Specification_simple(p):
    'Specification : STR SLASH STR'
    p[0] = CdaSpec('tag',p[1],p[3],0)

def p_Specification_complex(p):
    'Specification : STR IMPL STR IMPL NUMBER'
    p[0] = CdaSpec('dep',p[1],p[3],p[5])

# Error rule for syntax errors
def p_error(p):
    raise ParseException(p)

# Build the parser
parser = yacc.yacc()

if __name__ == "__main__":
    import sys
    if len(sys.argv) > 1: # we got some file to check
        for f in sys.argv[1:]:
            try:
                s = str(open(f).read())
            except EOFError:
                print("NOT FOUND: ", f)
                continue
            if not s:
                print("EMPTY: ", f)
                continue
            try:
                result = parser.parse(s)
                print("OK: ",f)
            except ParseException as e:
                sys.stderr.write(
                    "Error at line %i, wrong token %s\n"%
                    (e.line, e.token))
                print("UNPARSABLE: ",f)
            parser.restart()
    else:
        print('''
pass some files as command line arguments and this program will check them.
returns for each file <status>: <filename>

status can be
OK         the file is a valid .cda file
UNPARSABLE the file has the wrong format
NOT FOUND  the file wasn't found
EMPTY      the file is empty
''')

