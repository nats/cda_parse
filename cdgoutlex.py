# -*- coding:utf-8 -*-
# cdgoutlex - lexer for .cda files, used by cdgoutparse.
# Copyright (C) 2010  Arne Köhn <arne@arne-koehn.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import re
from ply import lex

tokens = (
    'COMMENT',
    'COLON',
    'BIIMPL',
    'STR',
    'NUMBER',
    'COMMA',
    'SLASH',
    'IMPL',
    'SEMICOLON'
    )

t_COLON   = r':'
t_BIIMPL  = r'<->'
t_COMMA   = r','
t_SLASH   = r'/'
t_IMPL    = r'->'
t_SEMICOLON = r';'


# no return value -> discard
def t_COMMENT(t):
    r'//[^\n]*|/\*(.|\n)*?\*/'
    pass

# A regular expression rule with some action code
def t_NUMBER(t):
    r'-?\d+'
    t.value = int(t.value)    
    return t

def t_STR(t):
    r"'(?:[^\\']|\\.)*'|[\w§]+"
    if t.value.startswith("'"):
        t.value = t.value[1:-1]
    # only single and double qoutes need to be unescaped
    t.value = t.value.replace(r'\"',r'"')
    t.value = t.value.replace(r"\'",r"'")
    return t

# Define a rule so we can track line numbers
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

# A string containing ignored characters (spaces and tabs)
t_ignore  = ' \t'

# Error handling rule
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

# Build the lexer
lexer = lex.lex(reflags=re.UNICODE)
